# Angular and PWA

`PWA` (progressive Web app) means a lot of different things.
From the developer's perspective, however, there are two things that make a web app a PWA:
`manifest.json` and service worker.
(One thing to note is that PWA works only with `https`. The only exception is `localhost` (for local testing, presumably).)

Angular now has a built-in mechanism for PWA support.



## How to Create a PWA with Angular

_(Note: PWA support was officially added in Angular 5.0 and Angular CLI 1.6.)_

Scaffold your new app with `--service-worker` flag:

    ng new my-new-pwa-app --service-worker



## How to Upgrade Your Angular App to PWA

In case you have an existing app (created before Angular 5/CLI 1.6), you can modify your app in 4 places:

1. Add `@angular/service-worker` to your `package.json`.
2. Add the `"serviceWorker": true` flag in `.angular-cli.json`.
3. Add `ServiceWorkerModule.register()` in your AppModule's imports.
4. Create `ngsw-config.json` file.

The easiest thing to do, at least for the first time, is to create a new empty app with `--service-worker` flag, and copy over the necessary files/lines to the existing app. This repo contains an empty Angular PWA app `angular-pwa` (created by CLI version 1.6.2) and you can use it as an example.


Note that the service worker is enabled only in the "prod" build.




## References

* [Getting Started with Progressive Web Apps](https://developers.google.com/web/updates/2015/12/getting-started-pwa)
* [Angular Service Worker - Step-By-Step Guide for turning your Application into a PWA](https://blog.angular-university.io/angular-service-worker/)
* [Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk)
* [Favicon Generator](https://realfavicongenerator.net/)
* [@angular/service-worker]()
* [@angular/cli]()
* []()


